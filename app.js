/*
 * @Date: 2021-07-31 13:27:54
 * @LastEditors: 熊明祥
 * @LastEditTime: 2021-08-01 18:08:20
 * @Description: 服务器
 */
const express = require('express');
const app = express();
const barData = require('./mock/data.json').barData;
const lineData = require('./mock/data.json').lineData;
const saleData = require('./mock/data.json').saleData;
const SocketServer = require('ws').Server;

//设置端口号
let server = app.listen(3000, () => {
    console.log(3000, '端口启动');
});


const wss = new SocketServer({ server });
//处理跨域
app.all('*', (req, res, next) => {
    res.setHeader('Access-Control-Allow-Origin', '*');
    res.setHeader('Access-Control-Allow-Headers', 'X-Requested-With');
    res.setHeader(
        'Access-Control-Allow-Methods',
        'PUT,POST,GET,DELETE,OPTIONS'
    );
    res.setHeader('X-Powered-By', ' 3.2.1');
    res.setHeader('Content-Type', 'application/json;charset=utf-8');
    next();
});

// 获取柱状图数据的接口
app.get('/get/getBarData', (req, res) => {
    const resData = {
        code: 200,
        message: 'Request is OK',
        result: {
            ...barData,
        },
    };
    res.send(resData);
});

// 获取折线图数据的接口
app.get('/get/getLineData', (req, res) => {
    const resData = {
        code: 200,
        message: 'Request is OK',
        result: {
            ...lineData,
        },
    };
    res.send(resData);
});

// 获取销售额数据的接口
app.get('/get/getSaleData', (req, res) => {
    const resData = {
        code: 200,
        message: 'Request is OK',
        result: {
            ...saleData,
        },
    };
    res.send(resData);
});

wss.on('connection', (connection) => {
    console.log('wss:连接上了');
    connection.on('message', (message) => {
        let res = JSON.parse(message);
        setInterval(() => {
            let barData = {
                xAxis: getData(12,13),
                yData: getData(12,1000),
            };
            let lineData = {
                xAxis: getData(12,13),
                yData: getData(12,1000),
            };
            let saleData = {
                total: Math.floor(Math.random() * 100000000),
                percent: Math.floor(Math.random() * 100),
            };
            const data = {
                barData,
                lineData,
                saleData,
            };
            connection.send(JSON.stringify(data));
        }, 3000);
    });
});

function getData(length,param) {
    let arr = [];
    for (var i = 0; i < length; i++) {
        arr.push(Math.floor(Math.random() * param));
    }
    return arr;
}
